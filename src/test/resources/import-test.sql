BEGIN;

CREATE EXTENSION IF NOT EXISTS unaccent;

COPY report(report_date,location,location_type,data_field,data_field_code,time_period,time_period_type,value,unit) FROM '/tmp/reports_clean.csv' DELIMITER ',' CSV HEADER;
--COPY location(id1, iso, country, id2, hasc, ccn, cca, blank, fortype, engtype, blank2, varname, geom) FROM '/tmp/locations.csv' DELIMITER E'\t';
COPY location(country, location, location_type, geom) FROM '/tmp/location_clean.csv' DELIMITER E'\t' CSV HEADER;
--ALTER TABLE location ADD COLUMN fortype_normalized VARCHAR;
--ALTER TABLE location ADD COLUMN hasc_normalized VARCHAR;

--UPDATE location SET fortype_normalized = unaccent(fortype);
--UPDATE location SET hasc_normalized = unaccent(hasc);

COMMIT;

