package org.launchcode.zikaDashboard.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.zikaDashboard.AbstractBaseRestIntegrationTest;
import org.launchcode.zikaDashboard.IntegrationTestConfig;
import org.launchcode.zikaDashboard.data.ReportRepository;
import org.launchcode.zikaDashboard.models.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class ReportRestControllerTests extends AbstractBaseRestIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ReportRepository reportRepository;

    @Test
    public void testGetSingleItem() throws Exception {


        mockMvc.perform(get("/api/reports?date=2017-03-01"))
                .andExpect(status().isOk());
    }


}
