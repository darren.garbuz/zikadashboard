package org.launchcode.zikaDashboard.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.zikaDashboard.IntegrationTestConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class IndexControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void contextLoads() throws Exception{
        this.mockMvc.perform(get("/")).andExpect(status().is2xxSuccessful())
        .andExpect(content().string(containsString("Zika Dashboard App")));

    }


}
