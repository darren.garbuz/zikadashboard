package org.launchcode.zikaDashboard.controllers;

import com.vividsolutions.jts.geom.Geometry;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.zikaDashboard.IntegrationTestConfig;
import org.launchcode.zikaDashboard.data.ReportRepository;
import org.launchcode.zikaDashboard.features.WktHelper;
import org.launchcode.zikaDashboard.models.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.closeTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class ReportControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ReportRepository reportRepository;

    @After
    public void tearDown(){
        reportRepository.deleteAll();
    }

    @Test
    public void reportPathWorks() throws Exception {
        this.mockMvc.perform(get("/report/")).andExpect(status().isOk());
    }



}
