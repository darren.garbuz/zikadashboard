package org.launchcode.zikaDashboard.data;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.zikaDashboard.IntegrationTestConfig;
import org.launchcode.zikaDashboard.features.WktHelper;
import org.launchcode.zikaDashboard.models.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.junit.Assert;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class ReportRepositoryTests {

    @Autowired
    ReportRepository reportRepository;

    @After
    public void tearDown(){
        reportRepository.deleteAll();
    }


    @Test
    public void findOne(){
        Report test = new Report("2016-04-02","Brazil-Rondonia","state",
                "zika_reported", "BR0011","NA", "NA",
                618.0,"cases");
        // WktHelper.wktToGeometry("POINT(0 0)"));
        Report testSaved = reportRepository.save(test);

        Assert.assertEquals(testSaved.getValue(),test.getValue(),0.0001);


    }

}
