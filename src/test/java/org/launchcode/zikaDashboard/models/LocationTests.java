package org.launchcode.zikaDashboard.models;

import org.junit.Assert;
import org.junit.Test;

public class LocationTests {
    @Test
    public void testReportConstructor() {

        Report test = new Report("2016-04-02","Brazil-Rondonia","state","zika_reported",
                "BR0011","NA", "NA",
                618.0,"cases");

        Assert.assertEquals(test.getReportDate(),"2016-04-02");
        Assert.assertEquals(test.getLocation(),"Brazil-Rondonia");
        Assert.assertEquals(test.getLocationType(),"state");
        Assert.assertEquals(test.getDataField(),"zika_reported");
        Assert.assertEquals(test.getDataFieldCode(),"BR0011");
        Assert.assertEquals(test.getTimePeriod(),"NA");
        Assert.assertEquals(test.getTimePeriodType(),"NA");
        Assert.assertEquals(test.getValue(),618,0.0001);
        Assert.assertEquals(test.getUnit(),"cases");
        //Assert.assertEquals(test.getLocationGeometry(),geometry);
        }
}
