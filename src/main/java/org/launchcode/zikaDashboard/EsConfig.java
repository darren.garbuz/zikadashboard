package org.launchcode.zikaDashboard;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class EsConfig {

    @Value("${es.index-name1}")
    private String reportIndexName;

    @Value("${es.index-name2}")
    private String locationIndexName;

    public String getReportIndexName() {
        return reportIndexName;
    }

    public void setReportIndexName(String reportIndexName) {
        this.reportIndexName = reportIndexName;
    }

    public String getLocationIndexName() {
        return locationIndexName;
    }

    public void setLocationIndexName(String locationIndexName) {
        this.locationIndexName = locationIndexName;
    }
}