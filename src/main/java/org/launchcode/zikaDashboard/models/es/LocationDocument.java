package org.launchcode.zikaDashboard.models.es;

import com.vividsolutions.jts.geom.Geometry;
import org.launchcode.zikaDashboard.features.Feature;
import org.launchcode.zikaDashboard.models.Location;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.HashMap;

@Document(indexName = "#{esConfig.locationIndexName}", type = "location")
public class LocationDocument {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;

    private Long locationId;
    private Feature feature;

    public LocationDocument() {}

    public LocationDocument(Location location) {

        HashMap<String, Object> properties = new HashMap<>();
        properties.put("countryState", location.getCountryState());
        Feature feature = new Feature(location.getGeom(),properties);

        this.locationId = location.getId();
        this.feature = feature;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public Feature getFeature() {
        return feature;
    }

    public void setFeature(Feature feature) {
        this.feature = feature;
    }
}