package org.launchcode.zikaDashboard.models.es;


import org.launchcode.zikaDashboard.models.Report;
import org.springframework.data.elasticsearch.annotations.*;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import static org.springframework.data.elasticsearch.annotations.FieldType.Keyword;
import static org.springframework.data.elasticsearch.annotations.FieldType.Text;

@Setting(settingPath = "/elasticsearch-settings.json")
@Document(indexName = "#{esConfig.reportIndexName}", type = "reports")
public class ReportDocument {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private String id;

    private Long reportId;

    @Field(type= Keyword)
    private String reportDate;

    @Field(type= Keyword)
    private String location;

    private String locationType;
    private String dataField;
    private String dataFieldCode;
    private String timePeriod;
    private String timePeriodType;
    private Double value;
    private String unit;


    public ReportDocument() {}

    public ReportDocument(Report report) {
        this.reportId = report.getId();
        this.reportDate = report.getReportDate();
        this.location = report.getLocation();
        this.locationType = report.getLocationType();
        this.dataField = report.getDataField();
        this.dataFieldCode = report.getDataFieldCode();
        this.timePeriod = report.getTimePeriod();
        this.timePeriodType =report.getTimePeriodType();
        this.value = report.getValue();
        this.unit = report.getUnit();
    }

    public Long getReportId() {
        return reportId;
    }

    public void setReportId(Long reportId) {
        this.reportId = reportId;
    }

    public String getReportDate() {
        return reportDate;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public String getDataField() {
        return dataField;
    }

    public void setDataField(String dataField) {
        this.dataField = dataField;
    }

    public String getDataFieldCode() {
        return dataFieldCode;
    }

    public void setDataFieldCode(String dataFieldCode) {
        this.dataFieldCode = dataFieldCode;
    }

    public String getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(String timePeriod) {
        this.timePeriod = timePeriod;
    }

    public String getTimePeriodType() {
        return timePeriodType;
    }

    public void setTimePeriodType(String timePeriodType) {
        this.timePeriodType = timePeriodType;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}