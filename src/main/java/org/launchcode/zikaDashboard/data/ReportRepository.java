package org.launchcode.zikaDashboard.data;

import org.launchcode.zikaDashboard.models.Report;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface ReportRepository extends JpaRepository<Report,Integer> {
    List<Report> findByReportDate(String reportDate);

    @Query(value = "SELECT DISTINCT report_date FROM report ORDER by report_date", nativeQuery = true)
    List<String> findAllReportDates();

}