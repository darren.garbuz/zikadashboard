package org.launchcode.zikaDashboard.data;

import org.launchcode.zikaDashboard.models.Location;
import org.launchcode.zikaDashboard.models.Report;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface LocationRepository extends JpaRepository<Location,Integer> {
    List<Location> findByCountryState(String countryState);

    //@Query(value = "SELECT country_state FROM location WHERE ':loc' LIKE CONCAT('%',country_state,'%')", nativeQuery = true)
    //List<Location> findLocationByLike(@Param("loc") String location);


}