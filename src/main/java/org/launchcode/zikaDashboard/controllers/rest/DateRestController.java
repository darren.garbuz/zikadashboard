package org.launchcode.zikaDashboard.controllers.rest;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.FuzzyQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramInterval;
import org.launchcode.zikaDashboard.data.ReportDocumentRepository;
import org.launchcode.zikaDashboard.data.ReportRepository;
import org.launchcode.zikaDashboard.models.es.ReportDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramInterval.DAY;

@RestController
@RequestMapping(value = "/api/dates")
public class DateRestController {

    @Autowired
    private ReportRepository reportRepository;

    @GetMapping
    public ResponseEntity getDates() {
        List<String> dates = reportRepository.findAllReportDates();
        return new ResponseEntity(dates, HttpStatus.OK);
    }

}
