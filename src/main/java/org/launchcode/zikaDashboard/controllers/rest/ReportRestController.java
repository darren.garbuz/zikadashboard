package org.launchcode.zikaDashboard.controllers.rest;

import com.vividsolutions.jts.geom.Geometry;
import org.launchcode.zikaDashboard.data.LocationRepository;
import org.launchcode.zikaDashboard.data.ReportRepository;
import org.launchcode.zikaDashboard.features.Feature;
import org.launchcode.zikaDashboard.features.FeatureCollection;
import org.launchcode.zikaDashboard.models.Location;
import org.launchcode.zikaDashboard.models.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/reports")
public class ReportRestController {
    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private LocationRepository locationRepository;

    @GetMapping
    public ResponseEntity getReports(@RequestParam Optional<String> date) {

        if(date.isPresent()){
            if(reportRepository.findByReportDate(date.get()) == null) {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
            else {
                FeatureCollection featureCollection = new FeatureCollection();

                List<Report> reports = reportRepository.findByReportDate(date.get());

                List<String> uniqueLocations = new ArrayList<>();
                HashMap<Geometry,ArrayList<Report>> reportMap = new HashMap<>();

                for(Report report : reports) {
                    String loc = report.getLocation();
                    List<Location> locations = locationRepository.findByCountryState(loc);

                    if(locations.size() > 0)
                    {
                        if(!uniqueLocations.contains(loc)){
                            uniqueLocations.add(loc);
                            ArrayList<Report> doc = new ArrayList<>();
                            doc.add(report);

                            reportMap.put(locations.get(0).getGeom(), doc);
                        }
                        else{
                            reportMap.get(locations.get(0).getGeom()).add(report);
                        }
                    }
                }

                for(Geometry geoKey : reportMap.keySet()){
                    HashMap<String, Object> properties = new HashMap<>();
                    properties.put("reports", reportMap.get(geoKey));
                    featureCollection.addFeature(new Feature(geoKey,properties));

                }

                return new ResponseEntity(featureCollection,HttpStatus.OK);
            }
        }
        else {
            return new ResponseEntity(reportRepository.findAll(), HttpStatus.OK);
        }

    }


}


