package org.launchcode.zikaDashboard.controllers.es;

import com.vividsolutions.jts.geom.Geometry;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.FuzzyQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.WildcardQueryBuilder;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;
import org.launchcode.zikaDashboard.data.LocationRepository;
import org.launchcode.zikaDashboard.data.ReportDocumentRepository;
import org.launchcode.zikaDashboard.features.Feature;
import org.launchcode.zikaDashboard.features.FeatureCollection;
import org.launchcode.zikaDashboard.models.Location;
import org.launchcode.zikaDashboard.models.es.ReportDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.ResultsExtractor;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping(value = "/api/reports")
public class ReportDocumentController {

    @Autowired
    private ReportDocumentRepository reportDocumentRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @GetMapping(value = "dates")
    public ResponseEntity getEsDates() {

        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .addAggregation(AggregationBuilders
                        .terms("uniqueDates")
                        .field("reportDate").size(10000))
                        .build();

        Aggregations aggregations = elasticsearchTemplate.query(searchQuery, new ResultsExtractor<Aggregations>() {
            @Override
            public Aggregations extract(SearchResponse response) {
                return response.getAggregations();
            }
         });

       Map<String, Aggregation> results = aggregations.asMap();
       String cat = "hi";

       StringTerms unique = (StringTerms) results.get("uniqueDates");

       List<String> keys = unique.getBuckets()
               .stream()
               .map(b -> b.getKeyAsString())
               .collect(toList());


        return new ResponseEntity(keys ,HttpStatus.OK);


    }

    @GetMapping(value = "locations")
    public ResponseEntity getEsLocations() {

        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .addAggregation(AggregationBuilders
                        .terms("uniqueLocations")
                        .field("location").size(10000))
                .build();

        Aggregations aggregations = elasticsearchTemplate.query(searchQuery, new ResultsExtractor<Aggregations>() {
            @Override
            public Aggregations extract(SearchResponse response) {
                return response.getAggregations();
            }
        });

        Map<String, Aggregation> results = aggregations.asMap();

        StringTerms unique = (StringTerms) results.get("uniqueLocations");

        List<String> keys = unique.getBuckets()
                .stream()
                .map(b -> b.getKeyAsString())
                .collect(toList());
        List<Location> locations = locationRepository.findAll();
        List<String> uniqueLocations = new ArrayList<>();

        for (Location location : locations) {
            if(!uniqueLocations.contains(location.getCountryState() )){
                uniqueLocations.add(location.getCountryState());
            }
        }

        List<String> inCommonKeys = new ArrayList<>();

        for (String key : keys) {
            if(uniqueLocations.contains(key)){
                inCommonKeys.add(key);
            }

        }

        return new ResponseEntity(inCommonKeys ,HttpStatus.OK);


    }

    @GetMapping(value = "search")
    public ResponseEntity search(@RequestParam String q) {



        WildcardQueryBuilder wildcardQueryBuilder = QueryBuilders.wildcardQuery("location","*"+ q + "*");

        List<ReportDocument> results = new ArrayList<>();
        Iterator<ReportDocument> iterator = reportDocumentRepository.search(wildcardQueryBuilder).iterator();

        Integer i = 1;

        while(iterator.hasNext() & (i <= 10000)) {

            results.add(iterator.next());
            i++;

        }

            FeatureCollection featureCollection = new FeatureCollection();

            List<String> uniqueLocations = new ArrayList<>();
            HashMap<Geometry,ArrayList<ReportDocument>> reportMap = new HashMap<>();


            for(ReportDocument reportDocument : results) {
                String loc = reportDocument.getLocation();

                List<Location> locations = locationRepository.findByCountryState(loc);

                if(locations.size() > 0){

                    if(!uniqueLocations.contains(loc)){
                        uniqueLocations.add(loc);
                        ArrayList<ReportDocument> doc = new ArrayList<>();
                        doc.add(reportDocument);

                        reportMap.put(locations.get(0).getGeom(), doc);

                    }

                    else{
                        reportMap.get(locations.get(0).getGeom()).add(reportDocument);
                    }

                }
            }

            for(Geometry geoKey : reportMap.keySet()) {
                HashMap<String, Object> properties = new HashMap<>();
                properties.put("reports", reportMap.get(geoKey));
                featureCollection.addFeature(new Feature(geoKey, properties));

            }

            return new ResponseEntity(featureCollection,HttpStatus.OK);

    }

}



