package org.launchcode.zikaDashboard.controllers;

import org.launchcode.zikaDashboard.data.ReportRepository;
import org.launchcode.zikaDashboard.features.Feature;
import org.launchcode.zikaDashboard.features.FeatureCollection;
import org.launchcode.zikaDashboard.models.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping(value = "/report")
public class ReportController {

    @Autowired
    private ReportRepository reportRepository;

    @RequestMapping(value = "/")
    @ResponseBody
    public FeatureCollection getReports()  {
        List<Report> reports;

        reports = reportRepository.findAll();

        if(reports.isEmpty()) { return new FeatureCollection(); }

        FeatureCollection featureCollection = new FeatureCollection();
        for (Report report : reports) {
            HashMap<String, Object> properties = new HashMap<>();
            properties.put("reportDate", report.getReportDate());
            properties.put("location", report.getLocation());
            properties.put("locationType", report.getLocationType());
            properties.put("dataField", report.getDataField());
            properties.put("dataFieldCode", report.getDataFieldCode());
            properties.put("timePeriod", report.getTimePeriod());
            properties.put("timePeriodType", report.getTimePeriodType());
            properties.put("value", report.getValue());
            properties.put("unit", report.getUnit());
//            featureCollection.addFeature(new Feature(report.getLocationGeometry(), properties));
        }
        return featureCollection;
    }
}




