BEGIN;

--CREATE EXTENSION IF NOT EXISTS unaccent;

--COPY report(report_date,location,location_type,data_field,data_field_code,time_period,time_period_type,value,unit) FROM '/tmp/reports_clean.csv' DELIMITER ',' CSV HEADER;
--COPY location(country_state, geom) FROM '/tmp/locations.csv' DELIMITER E'\t' CSV HEADER;

--UPDATE location set country_state = replace(country_state, 'Panama', 'Pama');
--UPDATE location set country_state = replace(country_state, ' ', '_');
--UPDATE location set country_state = 'Mexico-Guajuato' WHERE country_state = 'Mexico-Guanajuato';
--UPDATE location set country_state = 'Mexico-Siloa' WHERE country_state = 'Mexico-Sinaloa';
--UPDATE location set country_state = 'Mexico-Michoacan_de_Ocampo' WHERE country_state = 'Mexico-Michoacan';
--UPDATE location set country_state = 'Mexico-Coahuila_de_Zaragoza' WHERE country_state = 'Mexico-Coahuila';
--UPDATE location set country_state = 'Mexico-yarit' WHERE country_state = 'Mexico-Nayarit';
--UPDATE location set country_state = 'Mexico-Quinta_Roo' WHERE country_state = 'Mexico-Quintana_Roo';
--UPDATE location set country_state = 'Mexico-Veracruz_de_Igcio_de_la_Llave' WHERE country_state = 'Mexico-Veracruz';
--UPDATE location set country_state = 'Mexico-Queretaro_de_Arteaga' WHERE country_state = 'Mexico-Queretaro';

--UPDATE location set country_state = 'Brazil-Santa_Catari' WHERE country_state = 'Brazil-Santa_Catarina';
--UPDATE location set country_state = 'Brazil-Permbuco' WHERE country_state = 'Brazil-Pernambuco';
--UPDATE location set country_state = 'Brazil-Mis_Gerais' WHERE country_state = 'Brazil-Minas_Gerais';
--UPDATE location set country_state = 'Brazil-Amazos' WHERE country_state = 'Brazil-Amazonas';

COMMIT;

