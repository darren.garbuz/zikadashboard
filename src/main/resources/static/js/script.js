$(document).ready(function () {

    function update() {
           var element = document.getElementById("Progress_Status");
           if(element.style.visibility = "visible"){
            element.style.visibility = "hidden";
           }
           else{
            element.style.visibility = "visible";
           }
         }

    var request = new XMLHttpRequest()

    request.open('GET', '/api/reports/dates', true)
    request.onload = function() {
      // Begin accessing JSON data here
      var data = JSON.parse(this.response)

      data.sort()

      data.forEach( x => {

        $('#date-select').append(`<option value="${x}">${x}</option>`)
      })
    }
    request.send()

    var request2 = new XMLHttpRequest()

    request2.open('GET', '/api/reports/locations', true)
        request2.onload = function() {
          // Begin accessing JSON data here
          var data2 = JSON.parse(this.response)

          data2.sort()

          data2.forEach( x => {

            $('#esearch_test').append(`<option value="${x}">${x}</option>`)
          })
        }
        request2.send()



    const osmLayer = new ol.layer.Tile({
        source: new ol.source.OSM(),
        visible: true
    });

    const map = new ol.Map({
        target: 'mapPlaceholder',
        layers: [osmLayer],
        view: new ol.View({
            center: ol.proj.fromLonLat([260, 0]),
            zoom: 3
        }),
        controls: ol.control.defaults()
    });

    const polyStyle = new ol.style.Style({
              stroke: new ol.style.Stroke({
                color: 'rgb(0, 0, 0)',
                width: 1
              }),
              fill: new ol.style.Fill({
                color: 'rgba(104, 115, 150)'
              })
            })

    let reportSource = new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: '/api/reports?date=2015-11-28'
    });
    let reportLayer = new ol.layer.Vector({
        source: reportSource,
        style: [polyStyle],

    });

   const simpleWMS = new ol.layer.Image({

         source: new ol.source.ImageWMS({

           url: 'http://18.224.184.61:8989/geoserver/wms?service=WMS',
           params: {
             'LAYERS': 'ne:NE1_LR_LC_SR_W',
             'CRS': 'EPSG:4326',

           },
           ratio: 1
         })
       });

    map.addLayer(simpleWMS)
    simpleWMS.setVisible(false);
    map.addLayer(reportLayer);




       $('#myCheck').on("click",function(event) {

       const checkBox = document.getElementById("myCheck");

               // If the checkbox is checked, display the output text
               if (checkBox.checked == true){  simpleWMS.setVisible(true);}
               else {simpleWMS.setVisible(false);}

       })



    document.getElementById("Progress_Status").style.visibility = "visible"
                          document.getElementById("No_Data").style.visibility = "hidden"

                          var listenerKey = reportSource.on('change', function(e) {

                            if (reportSource.getState() == 'ready') {

                              update();
                              ol.Observable.unByKey(listenerKey);


                              if(reportLayer.getSource().getFeatures().length === 0) {
                                document.getElementById("No_Data").style.visibility = "visible"
                              }

                              if(reportLayer.getSource().getFeatures().length > 0) {
                                     map.getView().fit(reportLayer.getSource().getExtent(), (map.getSize()));
                               }


                              }

                          });


    $('select').on("change",function(event) {

            let newDate = $("select option:selected").text();
            map.removeLayer(reportLayer)

            reportSource = new ol.source.Vector({
                    format: new ol.format.GeoJSON(),
                    url: '/api/reports?date='+newDate
             });
            reportLayer = new ol.layer.Vector({
                     source: reportSource,
                     style: [polyStyle],

                });

              map.addLayer(reportLayer);

              document.getElementById("Progress_Status").style.visibility = "visible"
                                    document.getElementById("No_Data").style.visibility = "hidden"

                                    var listenerKey = reportSource.on('change', function(e) {

                                      if (reportSource.getState() == 'ready') {

                                        update();
                                        ol.Observable.unByKey(listenerKey);

                                        if(reportLayer.getSource().getFeatures().length === 0) {
                                          document.getElementById("No_Data").style.visibility = "visible"
                                        }

                                        if(reportLayer.getSource().getFeatures().length > 0) {
                                                                             map.getView().fit(reportLayer.getSource().getExtent(), (map.getSize()));
                                                                       }


                                        }

                                    });

        } );

            $('#searchClick').on("click",function(event) {

                    document.getElementById("searchClick").disabled = true;
                    document.getElementById("date-select").disabled = true;
                    document.getElementById("search").disabled = true;

                    update();

                    let newSearch = $("#search").val();
                    map.removeLayer(reportLayer)

                    reportSource = new ol.source.Vector({
                            format: new ol.format.GeoJSON(),
                            url: '/api/reports/search?q='+newSearch

                     });


                    reportLayer = new ol.layer.Vector({
                             source: reportSource,
                             style: [polyStyle],

                        });

                      map.addLayer(reportLayer);

                      document.getElementById("Progress_Status").style.visibility = "visible"
                      document.getElementById("No_Data").style.visibility = "hidden"

                      var listenerKey = reportSource.on('change', function(e) {

                        if (reportSource.getState() == 'ready') {
                            document.getElementById("searchClick").disabled = false;
                            document.getElementById("date-select").disabled = false;
                                                document.getElementById("search").disabled = false;

                          update();
                          ol.Observable.unByKey(listenerKey);

                          if(reportLayer.getSource().getFeatures().length === 0) {
                            document.getElementById("No_Data").style.visibility = "visible"
                          }

                          if(reportLayer.getSource().getFeatures().length > 0) {
                                                               map.getView().fit(reportLayer.getSource().getExtent(), (map.getSize()));
                                                         }


                          }

                      });


                      var listenerKey = reportSource.on('error', function(e) {


                                                  document.getElementById("searchClick").disabled = false;
                                                  document.getElementById("date-select").disabled = false;
                                                                      document.getElementById("search").disabled = false;

                                                update();
                                                ol.Observable.unByKey(listenerKey);


                                                  document.getElementById("No_Data").style.visibility = "visible"





                                            });



                } );

    map.on('click', function(event) {
            $('#reports').empty();
            //Add table headers
            $('#reports').append(`
                <tr>
                <th>Location</th>
                <th>Location Type</th>
                <th>Data Field</th>
                <th>Data Field Code</th>
                <th>Report Date</th>
                <th>Time Period</th>
                <th>Time Period Type</th>
                <th>Value</th>
                <th>Unit</th>
              </tr>`

            );

            map.forEachFeatureAtPixel(event.pixel, function(feature,layer) {

            const locationName = [];
            const locationNameTable = [];

            feature.get('reports').forEach(function(report,index) {

                if(locationName.includes(report.location) === false){
                    locationName.push(report.location);
                    locationNameTable.push(`
                        <tr>
                            <td class="featureLocation">${report.location}</td>
                            <td>${report.locationType}</td>
                            <td>${report.dataField}</td>
                            <td>${report.dataFieldCode}</td>
                            <td>${report.reportDate}</td>
                            <td>${report.timePeriod}</td>
                            <td>${report.timePeriodType}</td>
                            <td>${report.value}</td>
                            <td>${report.unit}</td>
                        </tr>`
                    );
                }
                else{
                    let index = locationName.indexOf(report.location);
                    locationNameTable[index] = `${locationNameTable[index]}
                        <tr>
                            <td></td>
                            <td></td>
                            <td>${report.dataField}</td>
                            <td>${report.dataFieldCode}</td>
                            <td>${report.reportDate}</td>
                            <td>${report.timePeriod}</td>
                            <td>${report.timePeriodType}</td>
                            <td>${report.value}</td>
                            <td>${report.unit}</td>
                        </tr>`
                 }

            } )

            locationNameTable.forEach(function (item) {
                $('#reports').append(item);
            });
     });
     })

     });

